/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __SIGNAL_HXX
#define __SIGNAL_HXX

/**
 * Signal handler for SIGKILL, SIGQUIT, SIGINT.
 * Raises a SIGUSR1 signal to tell nanohttp to stop.
 * @param sig signal to handle
 */
void handle_signal(int sig);

/**
 *  installs signal handlers (mostly SIG_IGN)
 */
void install_signal_handlers();

/**
 * Global variable used to determine if the program should halt. Will it ever halt? Only Alan Turing knows.
 * @see handle_signal()
 */
extern int exit_now;

#endif
