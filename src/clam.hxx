/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __CLAM_HXX
#define __CLAM_HXX

#include <confuse.h>
#include <pthread.h>
#include <string>

#include "config.hxx"

/**
 * Thread that reloads virus definitions as needed
 */
void *clam_refresh(void *v);

class clam {
      private:


      public:
/* TODO: make these private */

/**
 * Let's use know if clam_refresh is running or not.
 */
	bool refresh_thread_alive;

/**
 * A lock used to serialize access to the engine. Serialized access is
 * needed to keep clam_refresh() from changing the engine while
 * contains_virus() is using it.
 * @see clam_refresh()
 * @see contains_virus()
 */
	pthread_mutex_t engine_lock;

/**
 * Thread attributes used by the clam_refresh() thread. This is
 * a global so that main() can free them when its cleaning up.
 * @see main()
 * @see clam_refresh()
 */
	pthread_attr_t ta;

/**
 * Multiple threads are using and altering "engine".
 * Use the engine_lock to prevent concurrency issues.
 */
	struct cl_engine *engine;



/**
 * Load the virus definition files and prepare the engine.
 */
	 clam();

/**
 * Scans a file for virus.
 * @return -1 Error || 0 No Virus || +1 Virus Found
 */
	int clam_scan(std::string * filename, config * conf);

/**
 * Free resources used by the engine.
 */
	~clam();

};
#endif
