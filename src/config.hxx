/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __CONFIG_HXX
#define __CONFIG_HXX

#include <list>
#include <confuse.h>

/**
 * The default configuration file location.
 */
#define DEFAULT_CONFIGFILE "/etc/inoclam.conf"

/**
 * The configuration file path.
 */
extern char *configfile;

class config {
      private:
	char *directory;

	cfg_bool_t file_email_enabled;
	char *file_email_subject;

	cfg_bool_t virus_removal_enabled;
	cfg_bool_t virus_email_enabled;
	char *virus_email_subject;

	char *host;
	int port;
	char *from;

	 std::list < char *>*to;
      public:
	 config();
	~config();

	void setDirectory(char *new_dir);
	char *getDirectory();

	void setFileEMailEnabled(cfg_bool_t enabled);
	cfg_bool_t getFileEMailEnabled();
	void setFileEMailSubject(char *subject);
	char *getFileEMailSubject();

	void setVirusRemovalEnabled(cfg_bool_t enabled);
	cfg_bool_t getVirusRemovalEnabled();
	void setVirusEMailEnabled(cfg_bool_t enabled);
	cfg_bool_t getVirusEMailEnabled();
	void setVirusEMailSubject(char *subject);
	char *getVirusEMailSubject();

	char *getHost();
	void setHost(char *h);
	int getPort();
	void setPort(int p);
	char *getFrom();
	void setFrom(char *f);

	 std::list < char *>*getTo();
	void setTo(std::list < char *>*to);
};

/**
 * Parses an inoclam.conf configuration file.
 */
std::list < config::config * >*config_parse();

#endif
