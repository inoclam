/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __INOTIFY_HXX
#define __INOTIFY_HXX

#include <confuse.h>

#include "config.hxx"
#include "clam.hxx"

typedef struct {
	config *conf;
	clam *clamav;
} inotify_main_args_t;

/**
 * Watch the specified directory for changes and call contains_virus()
 * @param basedir the directory to watch.
 */
void *inotify_main(void *v);

#endif
