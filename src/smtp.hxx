/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __SMTP_HXX
#define __SMTP_HXX

#include <confuse.h>
#include <string>

#include "config.hxx"
#include "inoclam.hxx"

/**
 * Sends an E-Mail.
 */
void smtp_send(char *subject, std::string * body, config * config);

/**
 * Returns a banner to go at the bottom of e-mails. Remember to delete it when done.
 * @return footer string
 */
std::string * smtp_get_banner();

/**
 * Returns an RFC 2822 Compliant Date String
 * @return timestamp
 */
std::string * smtp_get_timestamp();

#endif
