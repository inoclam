// Note that the only valid version of the GPL as far as jwSMTP
// is concerned is v2 of the license (ie v2, not v2.2 or v3.x or whatever),
// unless explicitly otherwise stated.
//
// This file is part of the jwSMTP library.
//
//  jwSMTP library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; version 2 of the License.
//
//  jwSMTP library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with jwSMTP library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// jwSMTP library
//   http://johnwiggins.net
//   smtplib@johnwiggins.net
//
// Modified by Tom Cort <tom.cort@state.vt.us 30-Oct-2008.
// Renamed headers to .hxx and removed WIN32 specific code.

#ifndef __COMPAT_HXX__
#define __COMPAT_HXX__
    
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
typedef int SOCKET;
namespace jwsmtp {
	struct SOCKADDR_IN {
		sockaddr_in ADDR;	// we are wrapping this structure.
		
		    // this is bad as we just assume that the address "addr" is valid here.
		    // need a real check and set ok appropriately  
//  SOCKADDR_IN(sockaddr_in addr):ADDR(addr), ok(true) {}
		SOCKADDR_IN(const std::string & address, unsigned short port, short family = AF_INET) {
			ADDR.sin_port = port;
			ADDR.sin_family = family;
			ok = (inet_aton(address.c_str(), &ADDR.sin_addr));
		} SOCKADDR_IN(const SOCKADDR_IN & addr) {
			ADDR = addr.ADDR;
			ok = addr.ok;
		} SOCKADDR_IN operator =(const SOCKADDR_IN & addr) {
			ADDR = addr.ADDR;
			ok = addr.ok;
			return *this;
		}
		operator  bool()const {
			return ok;
		}
		operator  const sockaddr_in() const {
			 return ADDR;
		}
		operator  const sockaddr() const {
			 sockaddr addr;
			 std::copy((char *) &ADDR, (char *) &ADDR + sizeof(ADDR), (char *) &addr);
			return addr;
		}
		size_t get_size() const {
			return sizeof(ADDR);
		}
		char *get_sin_addr() {
			return (char *) &ADDR.sin_addr;
		} void set_port(unsigned short newport) {
			ADDR.sin_port = newport;
		} void set_ip(const std::string & newip) {
			ok = (inet_aton(newip.c_str(), &ADDR.sin_addr));
		} void zeroaddress() {
			ADDR.sin_addr.s_addr = 0;
	      } private:bool ok;
	};
	bool Connect(SOCKET sockfd, const SOCKADDR_IN & addr);
	bool Socket(SOCKET & s, int domain, int type, int protocol);
	bool Send(int &CharsSent, SOCKET s, const char *msg, size_t len, int flags);
	bool Recv(int &CharsRecv, SOCKET s, char *buf, size_t len, int flags);
	void Closesocket(const SOCKET & s);
}				// end namespace jwsmtp


#endif				//  #ifndef __COMPAT_HXX__
    
