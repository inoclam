/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <errno.h>
#include <idn-int.h>
#include <inotifytools/inotifytools.h>
#include <inotifytools/inotify.h>
#include <libdaemon/dlog.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <string>

#include "config.hxx"
#include "clam.hxx"
#include "inotify.hxx"
#include "monitor.hxx"
#include "signal.hxx"
#include "smtp.hxx"

/**
 * Watch the specified directory for changes and call contains_virus()
 * @param conf configuration
 */
void *inotify_main(void *v)
{
	inotify_main_args_t *args;
	struct inotify_event *event;
	int length;

	args = (inotify_main_args_t *) v;

	event = NULL;
	length = 0;

	if (args == NULL || args->conf == NULL || args->clamav == NULL || args->conf->getDirectory() == NULL) {
		daemon_log(LOG_ERR, "Invalid Arguments!");
		exit_now = 1;
		if (args) {
			free(args);
			args = NULL;
		}
		monitor_dec();
		return NULL;
	}

	if (!inotifytools_initialize() || !inotifytools_watch_recursively(args->conf->getDirectory(), IN_ALL_EVENTS)) {
		if (inotifytools_error() == ENOENT) {
			daemon_log(LOG_ERR, "directory '%s' does not exist. Check /etc/inoclam.conf", args->conf->getDirectory());
		} else {
			daemon_log(LOG_ERR, "Failed init inotify: %s", strerror(inotifytools_error()));
		}
		exit_now = 1;
	}

	daemon_log(LOG_INFO, "inotify watching '%s'", args->conf->getDirectory());

	while (!exit_now) {
		event = inotifytools_next_event(3);
		if (event && event->name && event->wd) {
			if ((event->mask & (IN_CLOSE_WRITE | IN_MOVED_TO)) && !(event->mask & IN_ISDIR)) {
				/*
				 * Scan writtable files that are being closed.
				 * Scan files that have been moved to a watched directory.
				 * TODO: run the scan in a new thread.
				 */

				std::string * filename;
				filename = new std::string();
				filename->append(inotifytools_filename_from_wd(event->wd), strlen(inotifytools_filename_from_wd(event->wd)));
				filename->append(event->name, strlen(event->name));

				/* Perform the Virus Checking */
				args->clamav->clam_scan(filename, args->conf);

				if (args->conf->getFileEMailEnabled() == cfg_true) {
					std::string * msg;
					msg = new std::string();

					msg->append("File: ");
					msg->append(filename->c_str());
					msg->append("\n");

					msg->append("Date: ");
					std::string * tstamp;
					tstamp = smtp_get_timestamp();
					if (!tstamp) {
						exit_now = 1;
						break;
					}
					msg->append(tstamp->c_str());
					delete tstamp;

					msg->append("\n\n");
					std::string * banner;
					banner = smtp_get_banner();
					msg->append(banner->c_str());
					delete banner;

					smtp_send(args->conf->getFileEMailSubject(), msg, args->conf);
					delete msg;	/* Clean up email msg */
				}

				delete filename;	/* Clean up filename */
			} else if ((event->mask & (IN_CREATE | IN_MOVED_TO | IN_DELETE)) && (event->mask & IN_ISDIR)) {
				/*
				 * If the directory structure has changed, reload the watch list.
				 * Benchmark this to determine if this is too much of a performance hit.
				 */
				inotifytools_cleanup();

				if (!inotifytools_initialize() || !inotifytools_watch_recursively(args->conf->getDirectory(), IN_ALL_EVENTS)) {
					if (inotifytools_error() == ENOENT) {
						daemon_log(LOG_ERR, "directory '%s' does not exist. Check /etc/inoclam.conf", args->conf->getDirectory());
					} else {
						daemon_log(LOG_ERR, "Failed init inotify: %s", strerror(inotifytools_error()));
					}
					exit_now = 1;
				}
			}

		}
	}

	inotifytools_cleanup();
	if (args) {
		free(args);
		args = NULL;
	}

	monitor_dec();
	return NULL;
}
