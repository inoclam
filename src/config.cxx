/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <list>
#include <confuse.h>
#include <libdaemon/dlog.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "config.hxx"
#include "clam.hxx"
#include "inotify.hxx"
#include "smtp.hxx"

config::config()
{
	directory = NULL;
	file_email_subject = NULL;
	virus_email_subject = NULL;
	host = NULL;
	from = NULL;
	to = NULL;
}

void config::setDirectory(char *new_dir)
{
	directory = new_dir;
}

char *config::getDirectory()
{
	return directory;
}

void config::setFileEMailSubject(char *subject)
{
	file_email_subject = subject;
}

char *config::getFileEMailSubject()
{
	return file_email_subject;
}

void config::setVirusEMailSubject(char *subject)
{
	virus_email_subject = subject;
}

char *config::getVirusEMailSubject()
{
	return virus_email_subject;
}

void config::setFileEMailEnabled(cfg_bool_t enabled)
{
	file_email_enabled = enabled;
}

cfg_bool_t config::getFileEMailEnabled()
{
	return file_email_enabled;
}

void config::setVirusEMailEnabled(cfg_bool_t enabled)
{
	virus_email_enabled = enabled;
}


cfg_bool_t config::getVirusEMailEnabled()
{
	return virus_email_enabled;
}

void config::setVirusRemovalEnabled(cfg_bool_t enabled)
{
	virus_removal_enabled = enabled;
}

cfg_bool_t config::getVirusRemovalEnabled()
{
	return virus_removal_enabled;
}

char *config::getHost()
{
	return host;
}

void config::setHost(char *h)
{
	host = h;
}

int config::getPort()
{
	return port;
}

void config::setPort(int p)
{
	port = p;
}

char *config::getFrom()
{
	return from;
}

void config::setFrom(char *f)
{
	from = f;
}

std::list < char *>*config::getTo()
{
	return to;
}

void config::setTo(std::list < char *>*t)
{
	to = t;
}

config::~config()
{
	if (to) {
		for (std::list < char *>::iterator itr = to->begin(); itr != to->end(); ++itr) {
			free(*itr);
		}
		delete to;
		to = NULL;
	}
	if (directory) {
		free(directory);
		directory = NULL;
	}

	if (virus_email_subject) {
		free(virus_email_subject);
		virus_email_subject = NULL;
	}

	if (file_email_subject) {
		free(file_email_subject);
		file_email_subject = NULL;
	}

	if (host) {
		free(host);
		host = NULL;
	}

	if (from) {
		free(from);
		from = NULL;
	}
}

/**
 * The configuration file path.
 */
char *configfile;

/**
 * Parses an inoclam.conf configuration file.
 */
std::list < config::config * >*config_parse()
{
	std::list < config::config * >*conf_list;
	conf_list = new std::list < config::config * >;

	int n, i, j;
	int rc;

	cfg_t *cfg;

	cfg_opt_t file_opts[] = {
		CFG_BOOL((char *) "email_alert", cfg_false, CFGF_NONE),
		CFG_STR((char *) "email_subject", (char *) "[inoclam] File Created/Changed", CFGF_NONE),
		CFG_END()
	};

	cfg_opt_t virus_opts[] = {
		CFG_BOOL((char *) "auto_remove", cfg_true, CFGF_NONE),
		CFG_BOOL((char *) "email_alert", cfg_true, CFGF_NONE),
		CFG_STR((char *) "email_subject", (char *) "[inoclam] Virus Detected", CFGF_NONE),
		CFG_END()
	};

	cfg_opt_t smtp_opts[] = {
		CFG_STR((char *) "host", (char *) "localhost", CFGF_NONE),
		CFG_INT((char *) "port", 25, CFGF_NONE),
		CFG_STR((char *) "from", (char *) "root@localhost", CFGF_NONE),
		CFG_STR_LIST((char *) "to", (char *) "{root@localhost}", CFGF_NONE),
		CFG_END()
	};

	cfg_opt_t watch_opts[] = {
		CFG_STR((char *) "directory", (char *) "/tmp", CFGF_NONE),
		CFG_SEC((char *) "file", file_opts, CFGF_NONE),
		CFG_SEC((char *) "virus", virus_opts, CFGF_NONE),
		CFG_SEC((char *) "smtp", smtp_opts, CFGF_NONE),
		CFG_END()
	};

	cfg_opt_t opts[] = {
		CFG_SEC((char *) "watch", watch_opts, CFGF_MULTI),
		CFG_END()
	};

	cfg = NULL;

	cfg = cfg_init(opts, 0);
	if (!cfg) {
		daemon_log(LOG_ERR, "cfg_init failed!");
		return conf_list;
	}

	rc = cfg_parse(cfg, configfile);
	if (rc == CFG_PARSE_ERROR) {
		daemon_log(LOG_ERR, "parser error '%s'", configfile);
	}

	n = cfg_size(cfg, "watch");
	for (i = 0; i < n; i++) {
		config::config * conf;
		conf = new config::config();

		cfg_t *smtp_cfg = NULL;
		cfg_t *file_cfg = NULL;
		cfg_t *virus_cfg = NULL;
		cfg_t *watch_cfg = cfg_getnsec(cfg, "watch", i);

		conf->setDirectory(strdup(cfg_getstr(watch_cfg, "directory")));

		file_cfg = cfg_getsec(watch_cfg, "file");
		conf->setFileEMailSubject(strdup(cfg_getstr(file_cfg, "email_subject")));
		conf->setFileEMailEnabled(cfg_getbool(file_cfg, "email_alert"));

		virus_cfg = cfg_getsec(watch_cfg, "virus");
		conf->setVirusEMailSubject(strdup(cfg_getstr(virus_cfg, "email_subject")));
		conf->setVirusEMailEnabled(cfg_getbool(virus_cfg, "email_alert"));
		conf->setVirusRemovalEnabled(cfg_getbool(virus_cfg, "auto_remove"));

		smtp_cfg = cfg_getsec(watch_cfg, "smtp");
		conf->setHost(strdup(cfg_getstr(smtp_cfg, "host")));
		conf->setPort(cfg_getint(smtp_cfg, "port"));
		conf->setFrom(strdup(cfg_getstr(smtp_cfg, "from")));


		std::list < char *>*to;
		to = new std::list < char *>;
		for (j = 0; j < cfg_size(smtp_cfg, "to"); j++) {
			to->push_front(strdup(cfg_getnstr(smtp_cfg, "to", j)));
		}
		conf->setTo(to);

		conf_list->push_front(conf);
	}

	if (cfg) {
		cfg_free(cfg);
		cfg = NULL;
	}

	return conf_list;
}
