/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <unistd.h>
#include <clamav.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libdaemon/dlog.h>
#include <pthread.h>
#include "clam.hxx"
#include "inoclam.hxx"
#include "monitor.hxx"
#include "signal.hxx"
#include "smtp.hxx"
#include "config.hxx"

#include <string>

/**
 * Thread that reloads virus definitions as needed
 */
void *clam_refresh(void *v)
{
	unsigned int sigs;
	int ret;
	struct cl_stat dbstat;

	clam *clamav;
	clamav = (clam *) v;

	memset(&dbstat, 0, sizeof(struct cl_stat));
	cl_statinidir(cl_retdbdir(), &dbstat);

	do {
		if (cl_statchkdir(&dbstat) == 1) {
			struct cl_engine *tmp_engine = NULL;
			struct cl_engine *old_engine = NULL;

			daemon_log(LOG_INFO, "Reloading new virus definitions");

			/* TODO: make options configurable. */
			/* For example: enable/disable CL_DB_NCORE, CL_DB_PHISHING_URLS, etc. */


			/* prepare the detection engine */
			tmp_engine = cl_engine_new();
			if (NULL == tmp_engine) {
				daemon_log(LOG_ERR, "cl_engine_new() error.");
				continue;
			}

			/* Load virus definition files */
			ret = cl_load(cl_retdbdir(), tmp_engine, &sigs, CL_DB_STDOPT);
			if (CL_SUCCESS != ret) {
				daemon_log(LOG_ERR, "cl_load() error: %s", cl_strerror(ret));
				cl_engine_free(tmp_engine);
				tmp_engine = NULL;
				continue;
			}

			daemon_log(LOG_INFO, "Virus definitions loaded (%d signatures).", sigs);

			ret = cl_engine_compile(tmp_engine);
			if (CL_SUCCESS != ret) {
				daemon_log(LOG_ERR, "cl_engine_compile() error: %s", cl_strerror(ret));
				tmp_engine = NULL;
			}

			daemon_log(LOG_INFO, "Virus detection engine ready.");

			/* Swap tmp_engine and engine, free resources from old engine */
			pthread_mutex_lock(&(clamav->engine_lock));
			old_engine = clamav->engine;
			clamav->engine = tmp_engine;
			tmp_engine = NULL;
			daemon_log(LOG_INFO, "Virus detection engine ready.");
			pthread_mutex_unlock(&(clamav->engine_lock));

			cl_engine_free(old_engine);
			old_engine = NULL;

			cl_statfree(&dbstat);
			memset(&dbstat, 0, sizeof(struct cl_stat));
			cl_statinidir(cl_retdbdir(), &dbstat);
		}

		sleep(5);
	} while (!exit_now);

	cl_statfree(&dbstat);

	monitor_dec();
	clamav->refresh_thread_alive = false;
	return NULL;
}

/**
 * Load the virus definition files and prepare the engine.
 */
clam::clam()
{
	unsigned int sigs = 0;
	int ret;

	pthread_t tt;

	engine = NULL;
	refresh_thread_alive = false;

	memset(&engine_lock, '\0', sizeof(pthread_mutex_t));
	pthread_mutex_init(&engine_lock, 0);
	pthread_mutex_lock(&engine_lock);

	/* Initialize libclamav */
	ret = cl_init(CL_INIT_DEFAULT);
	if (CL_SUCCESS != ret) {
		pthread_mutex_unlock(&engine_lock);
		daemon_log(LOG_ERR, "cl_init() error: %s", cl_strerror(ret));
		engine = NULL;
		return;
	}

	/* prepare the detection engine */
	engine = cl_engine_new();
	if (NULL == engine) {
		pthread_mutex_unlock(&engine_lock);
		daemon_log(LOG_ERR, "cl_engine_new() error.");
		return;
	}

	/* Load virus definition files */
	ret = cl_load(cl_retdbdir(), engine, &sigs, CL_DB_STDOPT);
	if (CL_SUCCESS != ret) {
		daemon_log(LOG_ERR, "cl_load() error: %s", cl_strerror(ret));
		cl_engine_free(engine);
		engine = NULL;
		return;
	}

	daemon_log(LOG_INFO, "Virus definitions loaded (%d signatures).", sigs);

	ret = cl_engine_compile(engine);
	if (CL_SUCCESS != ret) {
		daemon_log(LOG_ERR, "cl_engine_compile() error: %s", cl_strerror(ret));
		cl_engine_free(engine);
		engine = NULL;
		return;
	}

	daemon_log(LOG_INFO, "Virus detection engine ready.");
	pthread_mutex_unlock(&engine_lock);

	refresh_thread_alive = true;
	monitor_inc();
	pthread_attr_init(&ta);
	pthread_attr_setdetachstate(&ta, PTHREAD_CREATE_DETACHED);
	ret = pthread_create(&tt, &ta, clam_refresh, (void *) this);
	if (ret) {
		refresh_thread_alive = false;
		monitor_dec();
		daemon_log(LOG_ERR, "Can't create clam_refresh thread: %s", strerror(errno));
	}
}

/**
 * Scans a file for virus.
 * @return -1 Error || 0 No Virus || +1 Virus Found
 */
int clam::clam_scan(std::string * filename, config * conf)
{
	int ret;
	const char *virname;

	pthread_mutex_lock(&engine_lock);

	/* TODO: make options configurable. */
	/* For example: enable/disable CL_SCAN_BLOCKENCRYPTED, CL_SCAN_BLOCKMAX, CL_SCAN_OLE2, etc. */

	daemon_log(LOG_INFO, "About to scan '%s'", filename->c_str());

	ret = cl_scanfile(filename->c_str(), &virname, NULL, engine, CL_SCAN_STDOPT);
	if (CL_VIRUS == ret) {
		int file_removed = 0;

		pthread_mutex_unlock(&engine_lock);
		daemon_log(LOG_INFO, "%s: %s FOUND", filename->c_str(), virname);

		if (conf->getVirusRemovalEnabled() == cfg_true) {
			int rc;
			rc = unlink(filename->c_str());
			if (rc == 0) {
				file_removed = 1;
			} else {
				daemon_log(LOG_ERR, "unlink failed for '%s': %s", filename->c_str(), strerror(errno));
			}
		}

		if (conf->getVirusEMailEnabled() == cfg_true) {
			/* Sample Message:
			 * "File: <filename>\n"
			 * "Virus: <virname>\n"
			 * "Date: Thu, 28 Jun 2001 14:17:15 +0000\n"
			 * "Deleted: <Yes|No>\n"
			 * "\n"
			 * " (o_   Powered by: inoclam v1.0 (Bender)\n"
			 * " //\   Homepage:   http://www.inoclam.org/\n"
			 * " V_/_  Author:     Vermont Department of Taxes\n"
			 */

			std::string * smtp_body;
			smtp_body = new std::string();
			smtp_body->append("File: ");
			smtp_body->append(filename->c_str());
			smtp_body->append("\n");

			std::string * vname;
			vname = new std::string(virname);
			smtp_body->append("Virus: ");
			smtp_body->append(vname->c_str());
			smtp_body->append("\n");
			delete vname;

			std::string * tstamp;
			tstamp = smtp_get_timestamp();
			if (!tstamp) {
				return -1;
			}
			smtp_body->append("Date: ");
			smtp_body->append(tstamp->c_str());
			smtp_body->append("\n");
			delete tstamp;

			smtp_body->append("Deleted: ");

			std::string * rmstatus;

			if (file_removed == 1) {
				rmstatus = new std::string("Yes");
			} else {
				rmstatus = new std::string("No");
			}

			smtp_body->append(rmstatus->c_str());
			delete rmstatus;

			smtp_body->append("\n\n");

			std::string * banner;
			banner = smtp_get_banner();
			smtp_body->append(banner->c_str());
			delete banner;

			smtp_send(conf->getVirusEMailSubject(), smtp_body, conf);

			delete smtp_body;	/* Clean up email string */
		}

		return 1;
	} else if (CL_CLEAN == ret) {
		pthread_mutex_unlock(&engine_lock);
		daemon_log(LOG_INFO, "%s: OK", filename->c_str());
		return 0;
	} else {
		pthread_mutex_unlock(&engine_lock);
		daemon_log(LOG_ERR, "Scan Error: %s (%s)", cl_strerror(ret), filename->c_str());
		return -1;
	}
}

/**
 * Free resources used by the engine.
 */
clam::~clam()
{
	pthread_mutex_lock(&engine_lock);

	if (engine) {
		cl_engine_free(engine);
		engine = NULL;
	}

	while (refresh_thread_alive) {
		sched_yield();
		sleep(3);
	}

	pthread_mutex_unlock(&engine_lock);
	pthread_mutex_destroy(&engine_lock);
	pthread_attr_destroy(&ta);
}
