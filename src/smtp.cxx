/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <exception>
#include "jwsmtp.hxx"
#include <libdaemon/dlog.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include "smtp.hxx"

/**
 * Returns a banner to go at the bottom of e-mails. Remember to delete it when done.
 * @return footer string
 */
std::string * smtp_get_banner()
{
	/*
	 * " (o_   Powered by: inoclam v1.0 (Bender)\n"
	 * " //\   Homepage:   http://www.inoclam.org/\n"
	 * " V_/_  Author:     Vermont Department of Taxes\n"
	 */

	std::string * banner;
	banner = new std::string();
	banner->append(" (o_   Powered by inoclam v");

	std::string * version;
	version = new std::string(VERSION);
	banner->append(version->c_str());
	delete version;

	banner->append(" (");

	std::string * codename;
	codename = new std::string(CODENAME);
	banner->append(codename->c_str());
	delete codename;

	banner->append(")\n");

	banner->append(" //\\   Developed by the Vermont Department of Taxes\n");
	banner->append(" V_/_  Project Homepage: http://www.inoclam.org/\n");

	return banner;
}

/**
 * Returns an RFC 2822 Compliant Date String
 * @return timestamp
 */
std::string * smtp_get_timestamp()
{
	char *timestamp;
	const struct tm *tm;
	time_t now;

	now = time(NULL);
	tm = localtime(&now);

	timestamp = (char *) malloc(sizeof(char) * 41);
	if (!timestamp) {
		daemon_log(LOG_ERR, "malloc() failed!");
		return NULL;
	}

	memset(timestamp, '\0', 41);	/* RFC 2822 Date */
	strftime(timestamp, 40, "%a, %d %b %Y %H:%M:%S", tm);

	std::string * tstamp;
	tstamp = new std::string(timestamp);

	if (timestamp) {
		free(timestamp);
		timestamp = NULL;
	}

	return tstamp;
}

/**
 * Sends an E-Mail.
 */
void smtp_send(char *subject, std::string * body, config * conf)
{
	char *smtp_host = conf->getHost();
	int smtp_port = conf->getPort();
	char *smtp_from = conf->getFrom();
	const char *smtp_body = body->c_str();

	if (smtp_host == NULL || smtp_from == NULL || smtp_port < 1 || smtp_port > 65535 || subject == NULL || smtp_body == NULL) {
		daemon_log(LOG_INFO, "Incomplete Configuration or Message -- Not Sending E-Mail");
		return;
	}

	for (std::list < char *>::iterator itr = conf->getTo()->begin(); itr != conf->getTo()->end(); ++itr) {
		char *smtp_to;
		smtp_to = *itr;

		daemon_log(LOG_INFO, "Mailing '%s' ", smtp_to);

		/* Why are there no C libraries for sending mail that are this simple? */
		jwsmtp::mailer * mail;
		mail = new jwsmtp::mailer(smtp_to, smtp_from, subject, smtp_body, smtp_host, smtp_port, false);
		mail->send();
		delete mail;
	}

}
