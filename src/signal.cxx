/*
 * inoclam - Inotify+ClamAV virus scanner
 * Copyright (C) 2007, 2008, 2009 Vermont Department of Taxes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <libdaemon/dlog.h>
#include <signal.h>

#include "signal.hxx"

/**
 * Global variable used to determine if the program should halt. Will it ever halt? Only Alan Turing knows.
 * @see handle_signal()
 */
int exit_now;

/**
 * Signal handler for SIGKILL
 * @param sig signal to handle.
 */
void handle_sigkill(int sig)
{
	if (sig == SIGKILL) {
		exit_now = 1;
		daemon_log(LOG_INFO, "SIGKILL Caught ; preparing to exit");
	}
}

/**
 * Signal handler for SIGQUIT
 * @param sig signal to handle.
 */
void handle_sigquit(int sig)
{
	if (sig == SIGQUIT) {
		exit_now = 1;
		daemon_log(LOG_INFO, "SIGQUIT Caught ; preparing to exit");
	}
}

/**
 * Signal handler for SIGINT
 * @param sig signal to handle.
 */
void handle_sigint(int sig)
{
	if (sig == SIGINT) {
		exit_now = 1;
		daemon_log(LOG_INFO, "SIGINT Caught ; preparing to exit");
	}
}

/**
 *  installs signal handlers (mostly SIG_IGN)
 */
void install_signal_handlers()
{

	int i;

	for (i = 0; i < 32; i++) {
		if (i != SIGCHLD && i != SIGQUIT && i != SIGINT && i != SIGKILL) {
			signal(i, SIG_IGN);
		}
	}

	signal(SIGKILL, handle_sigkill);
	signal(SIGQUIT, handle_sigquit);
	signal(SIGINT, handle_sigint);
}
